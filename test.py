import os
import time
from init_vars import *
try:
	import RPi.GPIO as gpio
	gpio.setmode(gpio.BOARD)
	gpio.setup(tvpin, gpio.OUT)
	gpio.setup(isrunningledpin, gpio.OUT)
	gpio.setup(timerledpin, gpio.OUT)
	ispi = True
except ImportError:
	ispi = False
	print("WARNING: Couldn't load the RPi.GPIO module, GPIO functions won't work")
if ispi:
	print("Testing program status led...")
	gpio.output(isrunningledpin, gpio.HIGH)
	time.sleep(1)
	gpio.output(isrunningledpin, gpio.LOW)
	print("Testing cycle check led...")
	gpio.output(timerledpin, gpio.HIGH)
	time.sleep(1)
	gpio.output(timerledpin, gpio.LOW)
	print("Testing relay...")
	gpio.output(tvpin, gpio.HIGH)
	time.sleep(10)
def ringBell(startend):
	if ispi:
		gpio.output(tvpin, gpio.HIGH)
	time.sleep(5)
	if startend == "start":
		#writeLog("inf","Sending command to play end.wav")
		os.system(termcommand+" sound/start.wav")
	elif startend == "end":
		#writeLog("inf","Sending command to play start.wav" )
		os.system(termcommand+" sound/end.wav")
	elif startend == "warn":
		#writeLog("inf","Sending command to play warn.wav")
		os.system(termcommand+" sound/warn.wav")
	time.sleep(10)
	if ispi:
		gpio.output(tvpin, gpio.LOW)
print("Testing bell: in...")
ringBell("start")
time.sleep(10)
print("Testing bell: out...")
ringBell("end")
time.sleep(10)
print("Testing bell: warning...")
ringBell("warn")
time.sleep(5)
gpio.cleanup()
